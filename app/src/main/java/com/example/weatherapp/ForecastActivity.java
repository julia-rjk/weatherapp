package com.example.weatherapp;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ForecastActivity extends AppCompatActivity {
    CitiesDAO citiesDB;
    String city;
    TextView textWeather1,textWeather2,textWeather3,textWeather4,textWeather5;
    TextView textTemp1,textTemp2,textTemp3,textTemp4,textTemp5;
    TextView textDate1,textDate2,textDate3,textDate4,textDate5;
    ImageView image1, image2,image3,image4,image5;
    List<TextView> listTextWeather, listTextTemp, listTextDate;
    List<ImageView> listImageWeather;
    int i;

    String url;
    ProgressDialog dialog;
    SharedPreferences pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);

        init();

        pref = getPreferences(Context.MODE_PRIVATE);
        city = pref.getString("city", null);
        if (city == null) city = "Lyon";

        // Create DB
        citiesDB = new CitiesDAO(this);

        city = getIntent().getStringExtra("CITY");
        refreshData();
        animate();

    }

    public void init(){
        textWeather1 = findViewById(R.id.textWeather1);
        textWeather2 = findViewById(R.id.textWeather2);
        textWeather3 = findViewById(R.id.textWeather3);
        textWeather4 = findViewById(R.id.textWeather4);
        textWeather5 = findViewById(R.id.textWeather5);

        listTextWeather = new ArrayList<>();
        listTextWeather.add(textWeather1);
        listTextWeather.add(textWeather2);
        listTextWeather.add(textWeather3);
        listTextWeather.add(textWeather4);
        listTextWeather.add(textWeather5);
        //
        textTemp1 = findViewById(R.id.textTemp1);
        textTemp2 = findViewById(R.id.textTemp2);
        textTemp3 = findViewById(R.id.textTemp3);
        textTemp4 = findViewById(R.id.textTemp4);
        textTemp5 = findViewById(R.id.textTemp5);

        listTextTemp = new ArrayList<>();
        listTextTemp.add(textTemp1);
        listTextTemp.add(textTemp2);
        listTextTemp.add(textTemp3);
        listTextTemp.add(textTemp4);
        listTextTemp.add(textTemp5);
        //
        textDate1 = findViewById(R.id.textDate1);
        textDate2 = findViewById(R.id.textDate2);
        textDate3 = findViewById(R.id.textDate3);
        textDate4 = findViewById(R.id.textDate4);
        textDate5 = findViewById(R.id.textDate5);

        listTextDate = new ArrayList<>();
        listTextDate.add(textDate1);
        listTextDate.add(textDate2);
        listTextDate.add(textDate3);
        listTextDate.add(textDate4);
        listTextDate.add(textDate5);

        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);

        listImageWeather = new ArrayList<>();
        listImageWeather.add(image1);
        listImageWeather.add(image2);
        listImageWeather.add(image3);
        listImageWeather.add(image4);
        listImageWeather.add(image5);



    }


    void refreshData() {
        // Setting url
        url = "http://api.openweathermap.org/data/2.5/forecast?q=" + city + "&APPID=75fd394a27fc2eda3ab0b2ac936efdfd";


        //Weather data
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading....");
        dialog.show();

        StringRequest request = new StringRequest(url, new Response.Listener<String>() {
            // If it works we send the data to have it parsed
            @Override
            public void onResponse(String response) {
                parseJsonData(response);
            }
        }, new Response.ErrorListener() {
            // If it does'nt work
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplicationContext(), "Some error occurred!!", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        RequestQueue rQueue = Volley.newRequestQueue(ForecastActivity.this);
        rQueue.add(request);
    }

    // We parse json data to get city's name and temperature
    void parseJsonData(String jsonString) {
        try {
            JSONObject object = new JSONObject(jsonString);
            int j = 0, f= 0;

            boolean found = false;

            for(int i = 0; i < 5; i++) {
                JSONArray array = object.getJSONArray("list");
                JSONObject obj = object.getJSONArray("list").getJSONObject(j).getJSONObject("main");
                JSONObject obj2 = obj;
                String hour = object.getJSONArray("list").getJSONObject(j).getString("dt_txt");
                hour = hour.substring(11,19);

                int tempMax, tempMin;
                tempMax = (int) Math.round(obj.getDouble("temp") - 273.15);
                tempMin = (int) Math.round(obj.getDouble("temp") - 273.15);


                if(!found){
                    while(! hour.matches("00:00:00") && j < 39){
                        j = j+1;
                        hour = object.getJSONArray("list").getJSONObject(j).getString("dt_txt");
                        hour = hour.substring(11,19);

                        int tempObject = (int) Math.round(obj2.getDouble("temp") - 273.15);
                        if(tempObject < tempMin) tempMin = tempObject;
                        else if (tempObject > tempMax) tempMax = tempObject;

                    }


                    found = true;
                }



                f=j;
                String date = object.getJSONArray("list").getJSONObject(j).getString("dt_txt").substring(0,11);

                while (date.matches(object.getJSONArray("list").getJSONObject(f).getString("dt_txt").substring(0,11)) && f < 39){
                    obj2 =  object.getJSONArray("list").getJSONObject(f).getJSONObject("main");

                    int tempObject = (int) Math.round(obj2.getDouble("temp") - 273.15);
                    if(tempObject < tempMin) tempMin = tempObject;
                    else if (tempObject > tempMax) tempMax = tempObject;
                    f++;
                }


                SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
                Date dt1= null;
                try {
                    dt1 = format1.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat format2=new SimpleDateFormat("EEEE");
                String finalDay=format2.format(dt1);

                listTextDate.get(i).setText(finalDay+"\n"+date.substring(8)+"/ "+date.substring(5,7));


                int temperature = (int) Math.round(obj.getDouble("temp") - 273.15);

                String weather = object.getJSONArray("list").getJSONObject(j).getJSONArray("weather").getJSONObject(0).getString("main");
                listTextTemp.get(i).setText("max : " + tempMax +"°"+ "\nmin : "+ tempMin+"°");
                listTextWeather.get(i).setText(""+weather);

                // Depending on weather, we change weather's icon
                String path = "sun";
                switch (weather) {
                    case "Rain":
                        path = "rain";
                        break;
                    case "Clouds":
                        path = "cloud";
                        break;
                    case "Snow":
                        path = "snow";
                        break;
                    case "Clear":
                        path = "sun";
                        break;
                    default:

                        break;
                }
                ImageView imageWeather = listImageWeather.get(i);
                Context context = imageWeather.getContext();
                int resid = context.getResources().getIdentifier(path, "drawable", context.getPackageName());
                imageWeather.setImageResource(resid);

                if(j < 31) j = j + 8;
                else j = 39;
            }
            dialog.dismiss();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void animate(){
        i = 0;
        for(i = 0; i < listTextDate.size() ; i++){

                    listTextDate.get(i).setTranslationY(-200f);
                    listTextWeather.get(i).setTranslationY(-200f);
                    listTextTemp.get(i).setTranslationY(-200f);
                    listImageWeather.get(i).setTranslationY(-200f);

                    ObjectAnimator animation = ObjectAnimator.ofFloat(listTextDate.get(i), "translationY", 30f).setDuration(1000);
                    animation.start();
                    animation = ObjectAnimator.ofFloat(listTextWeather.get(i), "translationY", 30f).setDuration(1000);
                    animation.start();
                    animation = ObjectAnimator.ofFloat(listTextTemp.get(i), "translationY", 30f).setDuration(1000);
                    animation.start();
                    animation = ObjectAnimator.ofFloat(listImageWeather.get(i), "translationY", 30f).setDuration(1000);
                    animation.start();
        }

        }

    }
