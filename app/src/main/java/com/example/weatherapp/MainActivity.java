package com.example.weatherapp;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout layout;
    TextView textDate;
    String str, city, country, path;
    TextView textCity, textTemperature, textViewHour, textWeather, textTempMin, textTempMax;
    ProgressDialog dialog;
    String url;
    int currentMinutes, currentHour;
    Calendar rightNow;
    ImageView imageWeather;
    int resid;
    private static final String DEBUG_TAG = "MainActivity";
    CitiesDAO citiesDB;
    ArrayList<String> listCities;
    Menu menu;
    int idItem;
    NavigationView mNavigationView;
    SharedPreferences pref;
    ConstraintLayout constraintLayout;
    Button buttonNext;

    // https://samples.openweathermap.org/data/2.5/forecast?q=lyon,fr&APPID=75fd394a27fc2eda3ab0b2ac936efdfd

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        constraintLayout = findViewById(R.id.constraintLayout);


        // Displaying date
        textDate = findViewById(R.id.actualDate);

        // Create DB
        citiesDB = new CitiesDAO(this);

        //TextView
        textCity = findViewById(R.id.textCity);
        textTemperature = findViewById(R.id.textTemperature);
        textWeather = findViewById(R.id.textWeather);
        textTempMin = findViewById(R.id.textTemperatureMin);
        textTempMax = findViewById(R.id.textTemperatureMax);
        textViewHour = findViewById(R.id.textViewHour);

        buttonNext = findViewById(R.id.buttonNext);


        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/WalkwayBlack.ttf");

        mNavigationView = findViewById(R.id.navigation);

        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(this);
        }
        mNavigationView.getBackground().setAlpha(122);


        textWeather.setTypeface(custom_font);
        textCity.setTypeface(custom_font);
        textTemperature.setTypeface(custom_font);
        textTempMin.setTypeface(custom_font);
        textTempMax.setTypeface(custom_font);
        textDate.setTypeface(custom_font);
        textViewHour.setTypeface(custom_font);

        // Getting layout to change background
        layout = findViewById(R.id.navmenubar);

        // Weather's icon
        imageWeather = findViewById(R.id.imageWeather);


        // If this is the first time the user open the app then Lyon's weather will show
        // Otherwise it will be the last city seen
        pref = getPreferences(Context.MODE_PRIVATE);
        city = pref.getString("city", null);
        if (city == null) city = "Lyon";

        citiesDB.insertCity(city);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ForecastActivity.class);
                intent.putExtra("CITY", city);
                startActivity(intent);

            }
        });
        // We update data
        refreshData();
        idItem = 5;
        refreshMenu();
        animate();
    }

    @Override
    protected void onPause() {
        super.onPause();
        constraintLayout.setVisibility(View.INVISIBLE);

    }
    @Override
    public void onResume(){
        super.onResume();
        constraintLayout.setVisibility(View.VISIBLE);
        animate();
    }


    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == 100) {
            dialogChangeCity();
        }
        if (id == 200) {
            dialogInfo();
        }
        if (id == 300) {
            citiesDB.eraseData();
            refreshMenu();
        } else {
            city = (String) item.getTitle();
            refreshData();
        }

        return true;
    }


    public void dialogChangeCity() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Saisissez une ville");

        // Set up the input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                city = input.getText().toString();
                idItem++;
                if (citiesDB.insertCity(city)) refreshMenu();
                refreshData();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

    public void dialogInfo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Application météo développée par Julia ROJKOVSKA. :)");
        builder.show();
    }

    void refreshData() {
        // Setting url
        url = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&APPID=75fd394a27fc2eda3ab0b2ac936efdfd";

        //Date
        str = new SimpleDateFormat("dd MMMM", Locale.getDefault()).format(new Date());
        textDate.setText(str);

        //Time
        rightNow = Calendar.getInstance();
        currentHour = rightNow.get(Calendar.HOUR_OF_DAY);
        currentMinutes = rightNow.get(Calendar.MINUTE);

        if (currentMinutes < 10) textViewHour.setText("" + currentHour + ":0" + currentMinutes);
        else textViewHour.setText("" + currentHour + ":" + currentMinutes);
        // We change background's color depending on the current hour

        if (currentHour > 15 && currentHour < 19) {
            layout.setBackgroundResource(R.drawable.gradient_end_day);
        } else if (currentHour > 5 && currentHour < 18) {
            layout.setBackgroundResource(R.drawable.gradient_day);
        }else if (currentHour >= 18 || currentHour < 23) {
            layout.setBackgroundResource(R.drawable.gradient_evening);
        }else if (currentHour > 22 || currentHour < 6 ){
            layout.setBackgroundResource(R.drawable.gradient_night);
        }


        //Weather data
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading....");
        dialog.show();

        StringRequest request = new StringRequest(url, new Response.Listener<String>() {
            // If it works we send the data to have it parsed
            @Override
            public void onResponse(String response) {
                parseJsonData(response);
                commitPreferences();
            }
        }, new Response.ErrorListener() {
            // If it does'nt work
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplicationContext(), "Some error occurred!!", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        RequestQueue rQueue = Volley.newRequestQueue(MainActivity.this);
        rQueue.add(request);
    }

    // We parse json data to get city's name and temperature
    void parseJsonData(String jsonString) {
        try {
            JSONObject object = new JSONObject(jsonString);
            String name = object.getString("name");
            String country = object.getJSONObject("sys").getString("country");
            int temperature = (int) Math.round(object.getJSONObject("main").getDouble("temp") - 273.15);
            int tempMin = (int) Math.round(object.getJSONObject("main").getDouble("temp_min") - 273.15);
            int tempMax = (int) Math.round(object.getJSONObject("main").getDouble("temp_max") - 273.15);

            // We get description weather
            JSONArray array = object.getJSONArray("weather");
            JSONObject obj = array.getJSONObject(0);
            String weather = obj.getString("main");


            // Setting city
            if (name.matches("Arrondissement de Lyon")) name = "Lyon";
            // Setting country name
            if (country.matches("FR")) country = "France";

            textCity.setText(name + ", " + country);
            textTemperature.setText("" + temperature + "°");
            textTempMax.setText("" + tempMax + "°");
            textTempMin.setText("" + tempMin + "°");
            textWeather.setText("" + weather);

            // Depending on weather, we change weather's icon
            path = "sun";
            switch (weather) {
                case "Rain":
                    path = "rain";
                    break;
                case "Clouds":
                    path = "cloud";
                    break;
                case "Snow":
                    path = "snow";
                    break;
                case "Clear":
                    path = "sun";
                    break;
                default:
                    path = "cloud";
                    break;
            }
            // If it is the night then only a moon will show up
            if (currentHour > 19 || currentHour < 6) {
                path = "moon";
            }

            Context context = imageWeather.getContext();
            resid = context.getResources().getIdentifier(path, "drawable", context.getPackageName());
            imageWeather.setImageResource(resid);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog.dismiss();
        animate();
    }

    // We save the last city chosen
    public void commitPreferences() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("city", city);
        editor.commit();
    }

    public void refreshMenu() {
        menu = mNavigationView.getMenu();
        menu.clear();
        listCities = citiesDB.getAllCities();
        menu.add(1, 100, 0, "Ajouter une ville");
        menu.add(1, 200, 0, "A propos");
        menu.add(1, 300, 0, "Effacer");

        if (listCities.size() > 0) {

            for (int i = 0; i < listCities.size(); i++) {
                menu.add(0, i, 0, listCities.get(i));
            }
        } else if (listCities.size() == 0) {
            city = pref.getString("city", null);
            citiesDB.insertCity(city);
            listCities = citiesDB.getAllCities();
            menu.add(0, 0, 0, listCities.get(0));
        }
    }

    public void animate(){
        this.findViewById(R.id.textTemperature).setTranslationY(-200f);
        ObjectAnimator animation = ObjectAnimator.ofFloat(this.findViewById(R.id.textTemperature), "translationY", 30f);
        animation.setDuration(1000);
        animation.start();
    }

}





