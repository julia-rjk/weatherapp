package com.example.weatherapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class CitiesDAO extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "WeatherDB.db";
    public static final String TABLE_NAME = "cities";
    public static final String COLUMN_NAME = "name";

    public CitiesDAO(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table cities " +
                        "(id integer primary key, name text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS cities");
        onCreate(db);
    }

    public boolean insertCity (String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);

        Cursor res =  db.rawQuery( "select * from cities where name='"+name+"'", null );
        if(res.getCount() == 0){
            db.insert("cities", null, contentValues);

        }else{
            return  false;
        }
        return true;
    }


    // Return all cities in db
    public ArrayList<String> getAllCities() {
        ArrayList<String> listCities = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from cities", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            listCities.add(res.getString(res.getColumnIndex("name")));
            res.moveToNext();
        }
        return listCities;
    }

    public int removeCity (String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("cities",
                "name = ? ", new String[]{name});
    }

    public void eraseData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM cities");
        db.close();
    }

}
